/**
 * 
 */
package test.common.distributed.datasource.util;

import java.io.Serializable;

/**
 * @author liubing1
 * 
 */
public class CommonDatasourceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6789413085042915873L;

	private String tableName;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public CommonDatasourceBean(String tableName) {

		this.tableName = tableName;
	}

	public CommonDatasourceBean() {
		
	}
	
	
}
