/**
 * 
 */
package test.common.distributed.datasource.util;

import com.common.distributed.datasource.enums.RouteContextEnum;
import com.common.distributed.datasource.util.BeanPropertyAccessUtil;

import junit.framework.TestCase;

/**
 * @author liubing1
 *
 */
public class TestCommonDatasourceBean extends TestCase {
	/**
	 * 反射写入属性的名字
	 * @throws Exception
	 */
	public void testWrite() throws Exception{
		String tablename="demo";
		CommonDatasourceBean commonDatasourceBean=new CommonDatasourceBean();
		BeanPropertyAccessUtil.rewriteTableName(commonDatasourceBean, RouteContextEnum.tablename.toString(), tablename);
		System.out.println(commonDatasourceBean.getTableName());
	}
}
