/**
 * 
 */
package test.common.distributed.datasource.spring.config;

import java.util.Map;

import junit.framework.TestCase;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;
import com.common.distributed.datasource.spring.config.schema.DataBaseSchema;


/**
 * @author liubing
 *
 */
public class TestSrpingSchema  extends TestCase{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("springSchematest.xml");
		CommonDatasourceSchema clientSchema=(CommonDatasourceSchema) context.getBean("demo");
		Map<String, DataBaseSchema>result= clientSchema.getMap();
		System.out.println("type:"+clientSchema.getType());
		for(String key:result.keySet()){
			DataBaseSchema baseSchema=result.get(key);
			System.out.println("datasourceId:"+baseSchema.getDatasourceId());
			System.out.println("datasourceName:"+baseSchema.getDatabaseName());
			System.out.println(baseSchema.getTableSchemas().size());
		}
	}
	
}
