/**
 * 
 */
package com.common.distributed.datasource.spring.config;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.beans.factory.xml.AbstractSimpleBeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.util.xml.DomUtils;
import org.w3c.dom.Element;

import com.common.distributed.datasource.spring.config.schema.DataBaseSchema;
import com.common.distributed.datasource.spring.exception.SpringParseException;
/**
 * @author liubing1
 * 数据源解析
 */
public class DataBaseSchemaBeanParser extends
		AbstractSimpleBeanDefinitionParser {
	/**
	 * 
	 */
	public DataBaseSchemaBeanParser() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * spring schema 标签解析
	 */
	@Override
	protected void doParse(Element element, ParserContext parserContext,
			BeanDefinitionBuilder builder) {
		// TODO Auto-generated method stub
		try{
			
			 builder.addPropertyValue("tableSchemas", parseMapElement(element,  
	                    parserContext, builder)); 
			 builder.addPropertyValue("databaseName", element.getAttribute("databaseName")); 
			 builder.addPropertyValue("datasourceId", element.getAttribute("datasourceId")); 
		}catch(Exception e){
			throw new SpringParseException("DataBaseSchemaBeanParser doParse fail",e);
		}
	}
	/**
	 * 多个tablename标签处理
	 * @param mapEle
	 * @param parserContext
	 * @param builder
	 * @return
	 */
	
	@SuppressWarnings("unchecked")
	private ManagedList parseMapElement(Element mapEle,   
            ParserContext parserContext, BeanDefinitionBuilder builder){
		List<Element> entryEles = DomUtils.getChildElementsByTagName(mapEle, "tablename");
		ManagedList list=new ManagedList();
		list.setMergeEnabled(true);
		list.setSource(parserContext.getReaderContext().extractSource(mapEle));  
		
        for (Iterator it = entryEles.iterator(); it.hasNext();) {  
            Element entryEle = (Element) it.next();  
            list.add(parserContext.getDelegate().parseCustomElement(  
                    entryEle, builder.getRawBeanDefinition()));
  
        }
		return list;
		
	}
	
	@Override  
    protected Class<DataBaseSchema> getBeanClass(Element element) {  
        return DataBaseSchema.class;  
    } 
}
